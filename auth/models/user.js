const mongoose = require('mongoose');

const user = mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true,
        trim: true,
    },
    username: {
        type: String,
        required: true,
        unique: true,
        trim: true,
    },
    password: {
        type: String,
        required: true,
    },
});

module.exports = mongoose.model('user', user);
module.exports.modScema = user;