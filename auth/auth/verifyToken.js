const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const config = require('../config.js');
const User = require('../models/user');

const jwtOptions = {
    expiresIn: 86400,
};

function generateJwt(id) {
    return jwt.sign({id: id}, config.secret, jwtOptions);
}

function isJwtValid(token) {
    try {
        jwt.verify(token, config.secret, jwtOptions);
        return true;
    } catch (err) {
        return false;
    }
}

let register = (req, res, next)  => {
    if (req.body.password !== req.body.passwordConf) {
        return res.status(400).json({message: 'Passwords do not match.'});
    }

    const user = new User({
        email: req.body.email,
        username: req.body.username,
        password: bcrypt.hashSync(req.body.password, 10),
    });

    user
        .save()
        .then(data => {
            res.json({
                message: 'User has been successfully registered',
                auth: true,
                token: generateJwt(data._id),
            });
        })
        .catch(err => {
            console.log(err);
            res.status(500).json({message: 'Error while registering user.'});
        });
};

let login = (req, res, next) => {
    if (!req.body.logemail || !req.body.logpassword) {
        return res.status(400).json({message: 'All fields required.'});
    }

    User
        .findOne({
                email: req.body.logemail,
        }).then(user => {
            if (user && bcrypt.compareSync(req.body.logpassword, user.password)) {
                req.session.token = generateJwt(user._id);
                res.redirect('/profile');
            } else {
                res.status(401).json({message: 'Wrong email or password.'});
            }
        }).catch(err => {
            res.status(500).json(err.message);
        });
};

let profile = (req, res, next) => {
    let token = req.session.token;
    console.log(token);
    jwt.verify(token, config.secret, {ignoreExpiration: true}, (err, decoded) => {
        console.log(err);
        User
            .findById(decoded.id)
            .then(user => {
                res.json({
                    message: 'User logged in!',
                    username: user.username,
                    email: user.email,
                    auth: isJwtValid(token),
                    token: token,
                    userId: user._id,
                });
            }).catch(err => {
                console.log(err);
                res.status(500).json(err.message);
            });
    });
};

let logout = (req, res, next) => {
    req.session.token = null;
    res.redirect('/login');
};

module.exports = {
    register: register,
    login: login,
    profile: profile,
    logout: logout,
};
