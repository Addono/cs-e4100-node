const express = require('express');
const router = express.Router();

const auth = require('../auth/verifyToken');

router.get('/register', function(req, res, next) {
  res.json({message: 'Register User Page'});
});

router.post('/register', auth.register);

router.get('/login', function(req, res, next) {
    res.json({message: 'Login Page'});
});

router.post('/login', auth.login);

router.get('/profile', auth.profile);

router.get('/logout', auth.logout);


module.exports = router;