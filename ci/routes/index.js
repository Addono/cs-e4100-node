var express = require('express');
var router = express.Router();

const http = require('http');
const fs = require('fs');
const latex = require('node-latex');
var path = require('path');

/* GET home page. */
router.get('/', function (req, res, next) {
    let url = req.query.url;

    console.error("Receive request for " + url);

    const dir = path.join(__dirname, '..', 'files');
    const latexFilePath = path.join(dir, 'sample.tex');
    const pdfOutputPath = path.join(dir, 'final.pdf');

    console.error("LaTeX path\t" + latexFilePath);
    console.error("Output path\t" + pdfOutputPath);

    if (!fs.existsSync(dir)){
        console.error("Created dir\t" + dir);
        fs.mkdirSync(dir);
    }

    let file = fs.createWriteStream(latexFilePath);
    http.get(url, function (response) {
        let stream = response.pipe(file);

        stream.on('finish', () => {
            const input = fs.createReadStream(latexFilePath);
            const output = fs.createWriteStream(pdfOutputPath);

            try {
                latex(input)
                    .pipe(output)
                    .on('error', err => {
                        console.error(err);
                        res.json({status: "failure"});
                    })
                    .on('finish', () => {
                        console.error("Success");
                        res.json({status: "success"});
                    });
            } catch (err) {
                console.error(err);
                res.json({status: "failure", err: err});
            }
        });
    });
});

module.exports = router;
