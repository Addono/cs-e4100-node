var express = require('express');
const http = require("http");
var router = express.Router();

/* GET home page. */
router.get('/calc/:operator', function (req, res, next) {
    let first = req.query.first;
    let second = req.query.second;
    let message = null;

    if (first === undefined && second === undefined) {
        message = "Missing both parameters";
    } else if (first === undefined) {
        message = "Missing first required parameter";
    } else if (second === undefined) {
        message = "Missing second required parameter";
    }

    if (message != null) {
        res.status(400).json({message: message});
        return;
    }

    first = parseFloat(first);
    second = parseFloat(second);

    let firstNaN = isNaN(first);
    let secondNaN = isNaN(second);

    if (firstNaN && secondNaN) {
        message = "Both parameters are not numbers";
    } else if (firstNaN) {
        message = "The first parameter is not a number"
    } else if (secondNaN) {
        message = "The second parameter is not a number"
    }

    if (message != null) {
        res.status(400).json({message: message});
        return;
    }

    let result;
    switch (req.params.operator) {
        case 'add':
            result = first + second;
            break;
        case 'sub':
            result = first - second;
            break;
        case 'mul':
            result = first * second;
            break;
        case 'div':
            if (second === 0) {
                res.status(400).json({message: "Division by zero is not allowed"});
                return;
            } else {
                result = first / second;
            }
            break;
    }

    res.json({result: result.toFixed(3)});
});

router.get('/vappu/gettime', function (req, res, next) {
    let timeTillVappu = function (year) {
        return (new Date("04/31/" + year + " 03:00 +0300").getTime() - new Date().getTime()) / 1000;
    };

    let year = new Date().getFullYear();
    let seconds = timeTillVappu(year);

    if (seconds < 0) {
        seconds = timeTillVappu(year + 1);
    }

    res.json({seconds: Math.floor(seconds)});
});

router.get('/images', function (req, res, next) {
    var url = 'http://localhost:80';

    let jsonHandler = function (data) {
        var retrievedObject = JSON.parse(data);
        var result = {};

        console.error(retrievedObject);

        var retrieveImageData = function (path) {
            let req = http.get(url + path, function (response) {
                var body = '';

                response.on('data', function (chunk) {
                    body += chunk;
                });

                response.on('end', function () {
                    // your code here if you want to use the results !
                    result[path] = response.headers;

                    if (Object.keys(result).length >= retrievedObject.length) {
                        let output = [];
                        for (let key in result) {
                            if (result.hasOwnProperty(key)) {
                                output.push({
                                    name: result[key]["content-disposition"].substr("filename=".length),
                                    type: result[key]["content-type"].split("/")[1].toUpperCase(),
                                    size: result[key]["content-length"].substr(0, result[key]["content-length"].length-3) + "." + result[key]["content-length"].substr(-3) + " Kb"
                                });
                            }
                        }

                        console.error(output);

                        res.json(output);
                    }
                });
            }).end();
        };

        if (retrievedObject === null) {
            console.log("Could not retrieve JSON");
        } else {
            for (let i = 0; i < retrievedObject.length; i++) {
                retrieveImageData(retrievedObject[i].url);
            }
        }
    };

    var fs = require('fs'),
        path = require('path'),
        filePath = path.join(__dirname, '..', 'images.json');

    fs.readFile(filePath, {encoding: 'utf-8'}, function (err, data) {
        console.error("Data:\t" + data);
        if (!err) {
            console.log('received data: ' + data);
            jsonHandler(data);
        } else {
            console.log(err);
        }
    });
});

module.exports = router;
