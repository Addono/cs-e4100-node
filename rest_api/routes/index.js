var express = require('express');
var router = express.Router();

const Task = require('../models/taskModel');

/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.json({"message": "Hello " + req.query.name + "!"})
// });

// Create @ '/task'
router.post('/task', function(req, res, next) {
    // Retrieve the name param
    let taskData = {
        name: req.body.name,
    };

    // Optionally get the status param
    if (req.body.status) {
        taskData.status = req.body.status;
    }

    // Create a new task and store it in the database.
    const task = new Task(taskData);

    task
        .save()
        .then(data => {
            res.status(201).json({
                message: 'Task successfully created',
                id: data._id,
            });
        })
        .catch(err => {
            res.status(500).json(err);
        });
});

// Read @ '/tasks'
router.get('/tasks', function(req, res, next) {
    Task
        .find()
        .then(tasks => {
            res.json(tasks);
        }).catch(err => {
            res.status(500).json(err);
        });
});

// Read @ '/tasks/{task_id}'
router.get('/tasks/:taskId', function(req, res, next) {
    Task
        .findById(req.params.taskId)
        .then(tasks => {
            if (tasks) {
                res.json(tasks);
            } else {
                res
                    .status(404)
                    .json({
                        message: 'Task not found',
                        id: req.params.taskId,
                    });
            }
        }).catch(err => {
            res.status(500).json(err);
        });
});

// Update @ '/tasks/{task_id}'
router.put('/tasks/:taskId', function(req, res, next) {
    let values = {};

    if (req.body.name) {
        values.name = req.body.name;
    }
    if (req.body.created_date) {
        values.created_date = req.body.created_date;
    }
    if (req.body.status) {
        values.status = req.body.status;
    }

    Task
        .findByIdAndUpdate(
            req.params.taskId,
            values,
            {new: true}, // Return the new object to the then function.
        ).then(tasks => {
            if (tasks) {
                res
                    .json({
                        message: 'Task successfully updated',
                        id: tasks._id,
                        name: tasks.name,
                        created_date: tasks.created_date,
                        status: tasks.status,
                    });
            } else {
                res
                    .status(404)
                    .json({
                        message: 'Task not found',
                        id: req.params.taskId,
                    });
            }
    }).catch(err => {
            res.status(500).json(err);
    });
});

// Delete @ '/tasks/{task_is}'d
router.delete('/tasks/:taskId', function(req, res, next) {
    Task
        .findByIdAndDelete(req.params.taskId)
        .then(tasks => {
            if (tasks) {
                res.json({
                    message: 'Task successfully deleted',
                    id: req.params.taskId,
                });
            } else {
                res
                    .status(404)
                    .json({
                        message: 'Task not found',
                        id: req.params.taskId,
                    });
            }
        }).catch(err => {
            res.status(500).json(err);
        });
});


module.exports = router;
