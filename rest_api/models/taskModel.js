const mongoose = require('mongoose');

const tasks = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    created_date: {
        type: Date,
        default: Date.now,
    },
    status: {
        type: [String],
        enum: [['pending'], ['ongoing'], ['completed']],
        default: ['pending'],
    },
}, {
    timestamps: true
});

module.exports = mongoose.model('tasks', tasks);
module.exports.modScema = tasks